# frozen_string_literal: true

class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.string :title,            null: false, default: ''
      t.text :description,        default: ''
      t.integer :creator_id,      null: false
      t.integer :assigned_to_id,  null: false
      t.string :status,           null: false

      t.timestamps
    end
  end
end
