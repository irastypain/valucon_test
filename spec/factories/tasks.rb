# frozen_string_literal: true

FactoryBot.define do
  factory :task do
    title 'MyString'
    description 'MyText'
    creator_id 1
    assigned_to_id 1
  end
end
