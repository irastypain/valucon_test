# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe '#bootstrap_alert_class_for' do
    let(:types) do
      [{ type: :alert, class: 'warning' },
       { type: :error, class: 'danger' },
       { type: :notice, class: 'info' },
       { type: :success, class: 'success' }]
    end

    it 'returns bootstrap alert class by flash type' do
      types.each do |t|
        bootstrap_alert_class = bootstrap_alert_class_for(t[:type])
        expect(bootstrap_alert_class).to eq t[:class]
      end
    end
  end
end
