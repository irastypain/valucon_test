# frozen_string_literal: true

class Task < ApplicationRecord
  include AASM

  default_scope { includes(:creator, :assigned_to).references(:creator, :assigned_to) }
  scope :tasks_for, ->(user) { where(assigned_to: user) }
  scope :tasks_from, ->(user) { where(creator: user) }

  belongs_to :creator, class_name: 'User'
  belongs_to :assigned_to, class_name: 'User'

  validates :title, presence: true
  validates :creator, presence: true
  validates :assigned_to, presence: true

  aasm column: :status do
    state :new, initial: true
    state :started
    state :finished

    event :run do
      transitions from: :new, to: :started
    end

    event :complete do
      transitions from: :started, to: :finished
    end
  end

  def can_be_edited?(user)
    creator == user
  end

  def can_be_destroyed?(user)
    creator == user
  end

  def can_be_ran?(user)
    new? && assigned_to == user
  end

  def can_be_completed?(user)
    started? && assigned_to == user
  end
end
