# frozen_string_literal: true

module TasksHelper
  def action_show(task)
    link_to icon('fas', 'eye'), task, role: 'button',
                                      class: 'btn btn-sm btn-outline-info mr-1',
                                      title: 'Show'
  end

  def action_edit(task)
    link_to icon('fas', 'edit'), edit_task_path(task), role: 'button',
                                                       class: 'btn btn-sm btn-outline-warning mr-1',
                                                       title: 'Edit'
  end

  def action_destroy(task)
    link_to icon('fas', 'times'), task, data: { confirm: 'Are you sure?' },
                                        method: :delete,
                                        role: 'button',
                                        class: 'btn btn-sm btn-outline-danger mr-1',
                                        title: 'Destroy'
  end

  def action_run(task)
    link_to icon('fas', 'play', 'Start'), run_task_path(task), method: :put,
                                                               role: 'button',
                                                               class: 'btn btn-outline-success'
  end

  def action_complete(task)
    link_to icon('fas', 'flag-checkered', 'Finish'), complete_task_path(task), method: :put,
                                                                               role: 'button',
                                                                               class: 'btn btn-outline-primary'
  end
end
