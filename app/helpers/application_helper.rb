# frozen_string_literal: true

module ApplicationHelper
  def bootstrap_alert_class_for(flash_type)
    { alert: 'warning', error: 'danger', notice: 'info', success: 'success' }[flash_type.to_sym] || flash_type
  end

  def error_message_for(resource, field)
    resource.errors.messages[field].join(', ')
  end
end
