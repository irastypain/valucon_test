Rails.application.routes.draw do
  resources :tasks do
    member do
      put 'run', to: 'tasks#run'
      put 'complete', to: 'tasks#complete'
    end
  end

  devise_scope :user do
    post 'users/sign_up', to: 'users/registrations#create'
    put 'users/edit', to: 'users/registrations#update'
  end

  devise_for :users, controllers: {
    confirmations: 'users/confirmations',
    passwords: 'users/passwords',
    registrations: 'users/registrations',
    sessions: 'users/sessions',
    unlocks: 'users/unlocks'
  }

  root to: 'tasks#index'
end
