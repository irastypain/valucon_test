# frozen_string_literal: true

if ENV['COVERAGE']
  SimpleCov.start 'rails' do
    add_filter 'bin'
    add_filter 'config'
    add_filter 'vendor'

    add_group 'Channels', 'app/channels'
    add_group 'Controllers', 'app/controllers'
    add_group 'Helpers', 'app/helpers'
    add_group 'Jobs', 'app/jobs'
    add_group 'Libraries', 'lib'
    add_group 'Mailers', 'app/mailers'
    add_group 'Models', 'app/models'
  end
end
